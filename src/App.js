// import { useEffect, useState } from 'react';
// function App() {
//   const [inputValue, setInputValue]=useState("");

//   const [posts,setPosts]=useState([]);

//   const [currentPost,setCurrentPost]=useState(-1)

//   const handleInputValue = (event) => {
//       console.log(event.target.value);
//       setInputValue(event.target.value);
//     }
//   const addPost=(e)=>{
//     e.preventDefault();
//     if(currentPost==-1){
//       inputValue ? setPosts(prev=> [...prev,inputValue]) : alert("textni kiriting!");
//     }
//     else{
//       const updated=[...posts];
//       updated[currentPost]=inputValue;
//       setPosts(updated);
//       setCurrentPost(-1)
//     }
//     setInputValue("");
//   }
//   const deletPost=(id)=>{
//     //*yangi massiv ochib undagi indexga mos bo'lgan qiymatni o'chirishimiz kerak. eski massivni ustida o'chirish amalini qiksa react qiymatlarniemas massivni o'zini solishtirib o'zgarmagan deb o'ylaydi va biz tanlagan post o'chmaydi

//     const newPost=posts.filter((_,index)=>index!=id)
//     setPosts(newPost);

//   }
//   const editPost=(index)=>{
//     setInputValue(posts[index])
//     setCurrentPost(index);
//   }
//     return (
//       <div className='d-flex justify-content-center align-items-center'>
//         <div className="container bg-success-subtle px-5 py-4">
//           <h1 className='text-center'>TODO APP</h1>
//           <div className="row justify-content-center form">
//             <input type="text" placeholder='Things you want to do today?' className='col-8 rounded-2 border-success-subtle' onChange={handleInputValue} value={inputValue}/>
//             <button className='col-2 bg-success text-white' onClick={addPost}>{
//              currentPost==-1 ? "Add" : "Update"
//             }</button>
//           </div>
//           <div className='row mt-3'>
//             {
//               posts.map((item,index)=>{
//                 return <div key={index} className='col-10 offset-1 bg-white mt-2 d-flex justify-content-between p-2 align-items-center'>
//                   <p>{index}</p>
//                   <p>{item}</p>
//                   <button onClick={() => deletPost(index)} className='bg-info border border-info p-2 rounded-2 text-white'>delete</button>
//                   <button onClick={() => editPost(index)} className='bg-danger border-danger p-2 rounded-2 text-white'>edit</button>
//                 </div>
//               }   
//               )
//             }
//           </div>
//         </div>
//       </div>
//     )
// }
// export default App;
// //*html end tag label

// import React, { useState } from 'react'

// function App () {
//   const [posts,setPosts]=useState("")
//   const addPost=()=>{

//   }
//   return (
//    <>
//    <div className='w-100 d-flex justify-content-between p-5'>
//     <input placeholder='Enter the task' className='w-100 rounded-3 input'/>
//     <button className='px-5 py-2 rounded-3 btn btn-primary' onClick={addPost} >Add</button>
//    </div>
//    </>
//   )
// }
// export default App;




// import React, { createContext, useState } from 'react';
// import First from './first';
// export const optionalContext=createContext();
// const App = () => {
//   const [state,setState]=useState("Uzbek");
//   return (
//     <>
//     <optionalContext.Provider value={setState}>
//       {/* <p>Counter : {state}</p> */}
//       <div className='d-flex justify-content-between p-4 '>
//         <h1>Navbar</h1>
//         <button className='btn btn-dark'>{state}</button>
//       </div>
//     <First/>
//     </optionalContext.Provider>

//     </>
//   )
// }

// export default App

// import React, { useReducer } from 'react'
// import First from './first'
// const initialState={
//   count:0
// }
// function reducer(value,action){
//   if(action.type=="Add"){
//     return {count : value.count+action.payload}
//   }
//   else if(action.type=="Sub"){
//     return {count:value.count-action.payload}
//   }else if(action.type=="Double"){
//     return {count:value.count+action.payload}
//   }
//   else{
//     return value;
//   }
// }
// const App = () => {
//   const [counter,dispatch]=useReducer(reducer,initialState)
//   return (
//    <>
//    <p>Counter : {counter.count}</p>
//   <button onClick={()=>dispatch({type:"Add",payload:4})}>Add</button>
//   <button onClick={()=>dispatch({type:"Sub",payload:4})}>Sub</button>
//   <button onClick={()=>dispatch({type:"Double",payload:counter.count})}>Double</button>

//    {/* <First/> */}
//    </>
//   )
// }

// export default App

import React from 'react'
import { useState } from 'react';

const App = () => {
  const [inputValue, setInputValue] = useState("")
  const [posts, setPosts] = useState([])
  const [postIndex, setPostIndex] = useState(null)
  const handleInputValue = (e) => {
    setInputValue(e.target.value);
  }
  const UiDraw = () => {
    if(postIndex){
      const editing=posts.map((item,index)=>{
      if(postIndex-1==index){
        item=inputValue;
      }
      return item;
      })
      setPosts(editing);
      setPostIndex(null);
    }else{
      inputValue && setPosts(prev=>[...prev,inputValue])
    }
    setInputValue("")
  }
  const deletePost = (id) => {
    const filterPosts = posts.filter((_, index) => id != index);
    setPosts(filterPosts);
  }
  const editPost = (index) => {
    setPostIndex(index + 1)
    setInputValue(posts[index]);
  }
  return (
    <>
      <div className='d-flex justify-content-center mt-5'>
        <input className='w-50' onChange={handleInputValue} value={inputValue} />
        <button className='mx-3 px-5 btn btn-info' onClick={UiDraw}>{postIndex==null ? "add" : "editing"}</button>
      </div>
      <div className='row mt-3'>
        {
          posts.map((item, index) => {
            return <>
              <div className='col-10 offset-1 ' key={index}>
                <div className='d-flex justify-content-between mt-3'>
                  <h2>{index + 1} ) {item}</h2>
                  <button className='btn btn-danger' onClick={() => deletePost(index)} >delete</button>
                  <button className='btn btn-success' onClick={() => editPost(index)}>edit</button>
                </div>
              </div>
            </>
          })
        }
      </div>
    </>
  )
}

export default App
